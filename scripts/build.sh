#!/bin/bash

set -exu

ARCH=$(arch)
# Convert the architectures to the ones used by the containers
if [[ "${ARCH}" == "aarch64" ]]; then
  ARCH="arm64"
elif [[ "${ARCH}" == "x86_64" ]]; then
  ARCH="amd64"
else
  echo "ERROR: arch ${ARCH} not supported" >&2
  exit 1
fi

IMAGE_NAME="${IMAGE_NAME:-qa}"
IMAGE_VERSION="${IMAGE_VERSION:-0.1}"
DISTRO_NAME="${COMPOSE_DISTRO:-AutoSD}"
CONTAINER_IMAGE="${OCI_SOURCE:-quay.io/automotive-toolchain/autosd}"
CONTAINER_TAG="${OCI_SORCE_TAG:-img-minimal}"
PUBLISH_TAG="${CONTAINER_IMAGE}:${CONTAINER_TAG}-${ARCH}"

# Login
buildah login --username "${CONTAINER_REGISTRY}" --password "${CONTAINER_REGISTRY_PASSWORD}" "${CONTAINER_REGISTRY}"

# Build the image
buildah bud \
        --build-arg NAME="${DISTRO_NAME} - ${IMAGE_NAME}" \
        --build-arg VERSION="${IMAGE_VERSION}" \
        --build-arg CONTAINER_IMAGE="${CONTAINER_IMAGE}" \
        --build-arg CONTAINER_TAG="${CONTAINER_TAG}" \
        -f "${IMAGE_NAME}/Containerfile" \
        -t  "${PUBLISH_TAG}" \
        "${IMAGE_NAME}"

# Show the manifest
buildah inspect "${PUBLISH_TAG}"

# Push it to the registry
buildah push "${PUBLISH_TAG}"
